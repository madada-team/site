--- 
published: true
title: Les données essentielles…ou pas
layout: post
author: Ma Dada team
category: articles
tags: 
- madada
- cada

---

Lorsqu' une autorité publique souhaite acheter quelque chose 
ou verser une aide financière à une association, ou à une autre autorité publique, elle agit dans un **cadre réglementaire qui implique la publication d'informations essentielles à l'exercice démocratique** de ces activités. Nous avons donc utilisé Ma Dada pour vérifier si les autorités respectent bien leur obligation de transparence en la matière.

![Francis Picabia - Transparence (Tetes Paysage) (1928)](https://gitlab.com/madada-team/site/-/raw/site/_posts/images/picabia-transparence.jpg)

Il est possible d'assister aux délibérations publiques des assemblées représentatives ou de consulter régulièrement les sites internet sur lesquels les autorités publiques communiquent leurs besoins d'achats (marchés publics) ou leurs décisions d'octroi d'aides financières (conventions de subvention). Mais cela au prix d'un temps considérable et sans garantie de succès. 

**Depuis plus de dix ans, les citoyennes et citoyens demandent un meilleur accès aux données publiques**. Les fonctionnalités offertent par Madada++ permettent de faciliter l'exercice de ces demandes d'accès et de gagner énormément de temps en assurant l'envoi simultané à l'ensemble des destinataires. Il propose également un tableau de bord permettant de suivre leur avancement. Bien utile lorsque l'on part pour un marathon de plusieurs mois. 

Plusieurs lois et règlements européens ont définis un cadre incitant les autorités publiques à simplifier l'accès aux données et documents produits par les administrations et établissements publics. 

Afin d’appuyer ces démarches, l'Etat français a publié **plusieurs arrêtés relatifs aux données essentielles à la transparence de l’action publique** :

* [Arrêté du 17 novembre 2017 relatif aux conditions de mises à disposition des données essentielles des conventions de subvention](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000036040528/)

* [Arrêté du 22 mars 2019 relatif aux données essentielles dans la commande publique](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000038318675/)

Afin d'accompagner ce mouvement, plusieurs initiatives ont vu le jour en France pour **harmoniser la structuration des jeux de données concourant à une meilleure transparence de l'action publique**. L’État propose un espace de publication pour des schémas de données standardisés à portée nationale : [schema.data.gouv.fr](https://schema.data.gouv.fr). De son côté l’association Opendata France propose un [socle de schémas](https://scdl.opendatafrance.net/docs/) pour les données essentielles disponibles dans les autorités publiques locales comme les départements, les régions, les communes ou les communautés de communes.

En complément des **données budgétaires contenant le détail des recettes et dépenses réalisées pendant une année par une autorité publique (budget et compte administratif)**, ces jeux de données constituent le socle minimal de la transparence et de la capacité à rendre compte de l’usage de l’argent public par les administrations et les établissements publics.

![illustration transparence](https://gitlab.com/madada-team/site/-/raw/site/_posts/images/transparence.jpg)

En utilisant les fonctionnalités de Madada++ nous avons donc envoyé deux demandes à chacun des 96 départements de la France métropolitaine. Nous n'avons pas réussi à trouver d'adresse email de contact pour deux d'entre eux. Nous avons donc envoyé **188 demandes d'accès concernant soit la liste des titulaires et des montants des achats publics d'un montant supérieur à 40 000 € soit la liste des conventions dont le montant annuel est supérieur à 23 000€**. 

### La transparence de la commande publique au petit trot

Deux ans après la publication de l’arrêté de mars 2019 concernant la commande publique et à quelques mois de la fin des mandats des conseillers généraux, nous avons utilisé les fonctionnalités avancées de Madada ++ pour effectuer une évaluation de la mise en œuvre de ce dispositif réglementaire.

![illustration transparence](https://gitlab.com/madada-team/site/-/raw/site/_posts/images/viepublique.jpg)

Nous avons donc envoyés des demandes aux 94 départements pour lesquels nous avions pu récupérer une adresse email de contact en faisant référence au cadre réglementaire et aux schémas de standardisation suivant lequel nous souhaitions recevoir ces données.

**Après 4 mois incluant une démarche de recours gracieux quasiment systématique** (pas de réponse au bout du délai légal d’un mois), nous avons obtenu 36 réponses positives ou partiellement positives sur 94 départements auxquels nos demandes sont effectivement parvenues.
La carte ci-dessous représente les différents stades auxquels sont nos demandes dans Madada.

<iframe title="[ Données essentielles de la commande publique ]" aria-label="Carte" id="datawrapper-chart-ftQcq" src="https://datawrapper.dwcdn.net/ftQcq/3/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="686"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>

 La plupart du temps le jeu de données demandé ne nous a pas été transmis directement mais un lien vers la plate-forme d’échange entre les entreprises et l’autorité publique (appelée « place de marché » dans le jargon administratif) nous a été fourni. Par exemple [ici](https://marchespublics.eure.fr/?page=entreprise.EntrepriseRechercherListeMarches&search) pour le département de l'Eure. Il suffit ensuite de sélectionner l'entité administrative puis l'année et on obtient la liste des marchés publics conclus dans l'année et la possibilité d'exporter en XML la liste des résultats. 

Dans certains cas, ces interfaces applicatives contenaient une section ou un critère de sélection permettant de rechercher les fameuses données essentielles. Dans certains cas, la liste de résultats retournée était exportable au format [XML](https://marchespublics.eure.fr/app.php/api/v1/donnees-essentielles/contrat/xml-extraire-criteres/cg27/0/1/2020/false/false/false/false/false/false/false/false/false) ou Json suivant le formalisme défini par le législateur. 

Dans d’autres cas, ces données essentielles n’étaient consultables que de manière unitaire, ce qui signifie qu’au lieu de la liste consolidée demandée, il nous aurait fallu cliquer sur la fiche de chaque marché attribué pour récupérer les données essentielles et réaliser l’agrégation annuelle par nous-mêmes.

Dans un grand nombre de cas (36%) les autorités publiques n’ont pas répondu. Soit parce qu’elles ne le souhaitaient pas, soit parce qu’elles n’en avaient pas les moyens techniques et organisationnels. Soit...on ne sait pas car elles ne nous ont pas répondu. 

Cela pose le problème d’exercice du droit par les citoyennes et citoyens français de leur droit d’accès à l’information. Celui-ci dispose en effet que les administrations doivent nommer une personne responsable de l'accès aux documents administratifs (PRADA) [CRPA](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031367756). Cette disposition vise à garantir à chaque demandeuse ou demandeur que sa requête sera tratiée et qu"une réponse lui sera apportée. Une sorte de politesse administratrive.

**Malheureusement cette obligation n’est quasiment jamais mise en œuvre et très peu d’autorités publiques affichent sur leurs sites internet institutionnels les coordonnées de leur PRADA**.

Dans le cas où elles nous ont répondu, nous avons obtenu **36 réponses positives** dont 12 de la part des 30 conseils départementaux dirigés par un parti de gauche (40%) et 24 de la part des 64 conseils départementaux dirigés par un parti de droite (37,5 %). **L’absence de transparence de la commande publique n’est donc l’apanage d’aucune couleur politique**.

### Les subventions au pas

Concernant les subventions accordées le constat est encore pire : nous n’avons obtenu que **25 réponses positives** sur 94 demandes transmises (6 pour les départements de gauche soit 20% et 19 pour les départements de droite soit 30%). 
La carte ci-dessous représente l'état d'avancement de nos demandes.

<iframe title="[ Données essentielles des conventions de subventions ]" aria-label="Carte" id="datawrapper-chart-rND4Y" src="https://datawrapper.dwcdn.net/rND4Y/7/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="686"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>

Ce constat est d’autant plus amer que la production de cette liste des subventions accordées constitue une des bases du pilotage de leurs ressources financières par les autorités publiques. Cela devrait être le cas pour les besoins internes de contrôle de gestion de leur part. De plus ces données sont éxigées par l’État pour l’exercice de son contrôle de légalité. Elles figurent à ce titre en annexe des documents budgétaires transmis par les Conseils départementaux au trésor public pour contrôle de légalité et exécution.   

En effet, malgré l’organisation (théoriquement) décentralisée de l’action publique et l’autonomie (relative) financière des collectivités locales, elles ont l’obligation de transmettre à la préfecture de leur ressort territorial les décisions politiques votées par les élues et élus locaux en assemblée pour que celui-ci puisse juger de la légalité de leurs actions.

### La transparence financière, La France avec un bonnet d'âne ?

**En résumé sur 188 demandes formulées auprès des conseils départementaux, nous n'avons reçu que 61 réponses positives soit moins de 32%** 

Alors que la plupart des obligations faites aux autorités publiques locales en matière de transparence financière ont été réglementée par décret ou arrêté depuis 2 à 4 ans, certaines autorités publiques n’ont pas hésité à invoquer la crise sanitaire ou le coût de production des données demandées pour justifier leur incapacité à répondre positivement à nos demandes d’accès. 

À l'issue des échéances électorales, nous espérons voir les candidats s'engager sur les questions de transparence et de redevabilité des administration locales. Et nous invitons les citoyens à continuer à utiliser le site Ma Dada pour faire valoir leur droit d'accès à l'information publique. De notre côté, nous allons poursuivre nos investigations et saisir la commission d'accès aux documents administratifs afin d'essayer d'obtenir les données manquantes. 