--- 
published: true
title: Tutoriels vidéo Ma Dada
layout: post
author: Eda
category: articles
tags: 
- madada
- actualité
- tutoriel

---

L'équipe de Ma Dada a préparé deux courtes vidéos tutoriels pour vous aider à comprendre ce qu'est Ma Dada, comment créer un compte, comment faire votre première demande d'accès aux documents administratifs, comment utiliser la plateforme et comment aller plus loin dans vos démarches de demandes d'accès aux documents administratifs.

Voici les deux vidéos :
 - [Vidéo tutoriel 1](https://aperi.tube/w/9Bfj6KdE52M9Cj99HYhHAk) Qu'est-ce que Ma Dada?
 - [Vidéo tutoriel 2](https://aperi.tube/w/p/rUEnp7tfWYubEHNEzYiExU) : Comment faire une demande d'accès aux documents administratifs avec Ma Dada ?

Et le lien direct vers notre [playslist des deux tutoriels vidéo](https://aperi.tube/video-playlists/embed/d1cec9ca-0258-46a8-bc2d-6450b82599da) ici : 
<iframe title="Tutoriels Ma Dada" src="https://aperi.tube/video-playlists/embed/d1cec9ca-0258-46a8-bc2d-6450b82599da" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

Merci à [l'instance Peertube AperiTube](https://aperi.tube) de nous accueillir ❤️.

Cette série de vidéo tutoriels sera complétée prochainement par de nouvelles vidéos tutoriels <3

À très vite sur Ma Dada 🐎.

-- 

Ma  Dada (https://madada.fr) c'est aussi :
 * Un forum utilisateurs : https://forum.madada.fr
 * Un blog : https://madada.fr/blog
 * Mastodon : https://mamot.fr/@madadafr
 * Twitter : https://twitter.com/madadafr
 * Une chaîne vidéos Peertube : https://aperi.tube/a/madada
 * Du code source 100% libre : https://gitlab.com/madada-team
