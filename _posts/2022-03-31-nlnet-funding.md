--- 
published: true
title: "NGI zero discovery funding for Ma Dada from the Nlnet Foundation"
layout: post
author: Eda
category: articles
tags: 
- madada
- actualité
- atelier
- nlnet

---

In June 2021 Ma Dada applied for funding from the Nlnet Foundation's [NGI Zero Discovery](https://www.ngi.eu/ngi-projects/ngi-zero/) program. We were very happy to see our aplication accepted in october 2021. This funding gives Ma Dada the oportunity to have two days per week of paid work performed on our platform and tools. Of course this is not enough for all the work that our small team has to do in order for Ma Dada to keep going and keep developing. But this is a nice oportunity for us to :
* develop and implement some nice new features
* write a lot of documentation
* organise our community work
* produce tutorials (graphical and video work) on Ma Dada and on FOI right.

What we particularly love about this funding is the fact that we have to put all of our work on free and open source licences (which is exactly what we are doing since the beginning) and also we are encouraged to use and reuse free and open source tools to help us achieve our goals. This is like the ideal world where we would like to be <3.

Please check our [dedicated forum entry](https://forum.madada.fr/c/communication/nlnet-funding) for ongoing work on this funding and check this blog here for regular information on what's new with Ma Dada.

So far we have achieved the following most significant changes :
- [Statistics and data visualisation](https://madada.fr/stats) generated from our database, refreshed automatically every day (we used the free software called Metabase for this);
- [New video tutorials](https://aperi.tube/w/p/rUEnp7tfWYubEHNEzYiExU) on our Peertube Channel (a free open source alternative to Youtube);
- Our new [community forum](https://forum.madada.fr) is open to discussions, come and join us (based on the free open surce software Discourse);
- [Help pages on the website](https://madada.fr/aide/a_propos) have been updated and [further documentation](https://doc.madada.fr) installed (using free software mkdocs and gitlab pages);
- Our website has been updated with links to [our Mastodon account]() (a free and open source alternative to Twitter), links to our donation plateforms ([Helloasso](https://www.helloasso.com/associations/open-knowledge-france/collectes/ma-dada-2022-pariez-sur-nous) or [Liberapay)(https://liberapay.com/madada/donate), and all the above novelties.

Stay tuned for further news on Ma Dada and the Nlnet funding <3.
