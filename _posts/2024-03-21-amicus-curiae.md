---
published: true
title: Ma Dada intervient pour le droit d’accès aux informations publiques devant la Cour européenne des droits de l’homme 
layout: post
author: L'équipe Ma Dada
category: articles
tags:
- ma dada
- demandes d'accès
- cedh
- amicus curiae
- pierre januel
---

*Par Xavier Berne, délégué général de Ma Dada* 


**L’association Open Knowledge France, qui œuvre pour le droit d’accès aux informations publiques (notamment par l’intermédiaire de Ma Dada) vient de déposer un amicus curiae auprès de la Cour européenne des droits de l’homme. Ce document intervient en soutien à un recours déposé par le journaliste Pierre Januel. Son objectif : dénoncer les lacunes du droit français en matière d’accès aux documents administratifs, et mettre en exergue les difficultés rencontrées de manière systémique par les demandeurs.**

En 2019, le journaliste Pierre Januel a tenté d’obtenir la liste des participants aux « chasses présidentielles » organisées au Château de Chambord, établissement public. Une demande qui permettait d’éclairer à la fois les activités d’une organisation financée au moins en partie grâce à des fonds publics, mais aussi les activités de représentation de responsables publics (notamment des parlementaires participant aux battues). 

Cette requête s’est toutefois heurtée à des refus successifs. D’abord, celui du domaine national de Chambord, confirmé successivement par la Commission d’accès aux documents administratifs (CADA), puis le juge administratif et par le Conseil d’État. Motif invoqué ? Le droit au respect de la vie privée des participants à ces chasses.

Le journaliste a alors formé une requête devant la Cour européenne des droits de l’homme (CEDH) pour atteinte à son droit de collecter des informations en vue de réaliser une enquête journalistique sur un sujet d’intérêt général - obstruction ayant empêché le public de recevoir de telles informations.

### Un droit lacunaire et massivement ignoré des administrations

Pour Open Knowledge France, cette intervention était justifiée à deux titres.

Premièrement, parce que le droit français ne prévoit pas de dispositif de « mise en balance » entre les secrets protégés par la loi (ici, le droit au respect de la vie privée) et l’intérêt pour le public de connaître certaines informations. Or, cette recherche d’équilibre est prévue par des textes européens et est justifiée dans toute société démocratique au sens de la jurisprudence de la CEDH. 

Deuxièmement, parce qu’exercer son droit d’accès aux informations publiques relève aujourd’hui en France d’un véritable parcours du combattant. Les statistiques de la plateforme Ma Dada confirment ce constat, déjà établi à de nombreuses reprises, notamment par des rapports parlementaires. Près de 90 % des demandes effectuées par l’intermédiaire de Ma Dada n’obtiennent par exemple aucune réponse (pas même un accusé de réception) à l’issue du délai légal d’un mois. Toutes ces difficultés procédurales découragent bien souvent les demandeurs, quand bien même leurs requêtes sont parfaitement légitimes et justifiées en droit.

Vous pouvez retrouver l’intégralité de notre argumentaire ici : [madada.frama.space/s/onmJCrGNmnYcGNo](https://madada.frama.space/s/onmJCrGNmnYcGNo)

Nous espérons que nos observations nourriront utilement les débats devant la CEDH, dans le but de faire progresser le droit d’accès aux informations publiques. 
