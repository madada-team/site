---
published: true
title: Retour sur la transparence (à géométrie variable) des notes de frais des maires
layout: post
author: L'équipe Ma Dada
category: articles
tags:
- ma dada
- demandes d'accès
- notes de frais
 maires
---

*Par Xavier Berne, délégué général de Ma Dada* 



En mars dernier, j’ai demandé - à titre personnel - aux 30 plus grandes villes de France (hors Paris) les notes de frais - de représentation, de déplacement et de restauration - de leurs maires. Ceci grâce à Ma Dada bien entendu, mais surtout à l’aune d’un [récent arrêt du Conseil d’État](https://blog.madada.fr/articles/2023/02/14/notes-de-frais.html), qui a condamné la ville de Paris suite à une demande similaire du journaliste Stefan de Vries.  


Trois mois plus tard, on constate que neuf villes ont répondu favorablement à ces demandes de communication de “documents administratifs” : 
- [Aix-en-provence](https://madada.fr/demande/notes_de_frais_maire_11#incoming-3017)
- [Amiens](https://madada.fr/demande/notes_de_frais_maire_12)
- [Annecy](https://madada.fr/demande/notes_de_frais_maire_14#incoming-3223)
- [Boulogne-Billancourt](https://madada.fr/demande/notes_de_frais_maire_15#incoming-3326)
- [Grenoble](https://madada.fr/demande/notes_de_frais_maire_19#incoming-5070)
- [Nantes]( https://madada.fr/demande/notes_de_frais_maire_7)
- [Rennes](https://madada.fr/demande/notes_de_frais_maire_28#incoming-4557)
- [Strasbourg](https://madada.fr/demande/notes_de_frais_maire_9#incoming-3117)
- [Toulon](https://madada.fr/demande/notes_de_frais_maire_31#incoming-3747)  



Les 21 villes restantes n’ont malheureusement toujours pas donné suite, alors que le droit prévoit que tout silence conservé pendant un mois équivaut à un refus implicite. J’ai d’ailleurs saisi la Commission d’accès aux documents administratifs (CADA) dans l’espoir que l’autorité administrative incite les communes concernées à publier les notes de frais de leurs maires.



### Que retenir de cette initiative ? 



Déjà, que **le droit d’accès aux documents administratifs peut fonctionner !** On ne peut que féliciter les villes ayant “joué le jeu” de la transparence, surtout pour ces informations publiques au caractère malgré tout (très) sensible politiquement. 



Sur le fond, on a vu que certaines dépenses avaient beaucoup fait réagir,  [notamment du côté d’Aix-en-Provence](https://www.francebleu.fr/infos/politique/video-notes-de-frais-devoilees-j-assume-parfaitement-assure-la-maire-d-aix-en-provence-2757384) (pour des dépenses en parfums, en esthétique…). **Chacun jugera, et c’était aussi l’un des objectifs de cette initiative : créer du débat sur les frais de mandat de nos élus**.   



Pour ma part, j’estime que la transparence a un double effet vertueux : il permet aux élus de mieux mettre en avant la réalité de leur travail (déjeuners de travail, déplacements professionnels, etc.) ; il apporte un contrôle citoyen, favorable à une meilleure utilisation des deniers publics.


  
Enfin, et j’ai envie de dire surtout, il me semble **profondément regrettable que les 21 maires n’ayant pas donné suite à ces demandes de transparence n’aient pas eu à assumer autant de remontrances** que ceux s’étant pliés à leurs obligations légales… Sont ainsi concernés :   

- Angers 
- Bordeaux 
- Brest 
- Clermont-Ferrand 
- Dijon 
- Le Havre
- Le Mans  
- Lille 
- Limoges 
- Lyon 
- Marseille
- Montpellier  
- Nice 
- Nîmes 
- Perpignan 
- Reims 
- Saint-Denis 
- Saint-Etienne 
- Toulouse 
- Tours 
- Villeurbanne  



### Un révélateur du potentiel offert par le droit d’accès aux informations publiques 



J’espère que cette initiative permettra aux acteurs de terrain (journalistes, associations, élus…) de **s’emparer de ces informations publiques, mais aussi de mieux saisir le potentiel, pour l’intérêt général, offert par le droit d’accès aux documents administratifs**.   



Pour prendre un peu de recul, soulignons que la question des notes de frais s’avère finalement assez anecdotique au regard du spectre couvert par le fameux “droit CADA” : ce dernier permet d’accéder à des **informations très précieuses, notamment au niveau local, sur l’attribution de marchés publics, sur la construction des décisions publiques (rapports, analyses préalables, etc.), sur l’efficacité des politiques publiques (données statistiques, etc.)**.   

Le rôle de la société civile est de ce fait très important pour l’ouverture de ces informations d’intérêt public : les acteurs de terrain sont souvent les mieux placés pour connaître les décisions ayant d’importantes implications pour leur territoire. C’est en cela que la plateforme associative et citoyenne Ma Dada me semble un précieux **maillon de la chaîne : il accompagne les demandeurs et facilite leur droit d’accès, tout en permettant par la suite une libre diffusion des documents obtenus**. Des documents demandés *par* des citoyens, *pour* des citoyens ! 

