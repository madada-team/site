--- 
published: true
title: Vidéo du webinaire MaDada++ du 2 mars
layout: post
author: Eda
category: articles
tags: 
- madada
- actualité

---

La vidéo de notre webinaire du 2 mars 2021, où nous expliquions comment utiliser MaDada, et quelles sont les nouvelles fonctionnalités offertes dans la version MaDada++, est [disponible sur notre nouvelle chaîne Peertube](https://aperi.tube/videos/watch/ff5e7dad-420f-4b8a-82d7-81bc3aa28616). 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://aperi.tube/videos/embed/ff5e7dad-420f-4b8a-82d7-81bc3aa28616" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Merci à [l'instance AperiTube](https://aperi.tube) de nous accueillir ❤️.

L'enregistrement ne comporte ni les noms des personnes présentes (en dehors des membres de l'équipe), ni la dernière partie du webinaire avec les questions-réponses du public, ceci afin de préserver l'intimité et l'anonymat de chacun. 

À très vite sur Ma Dada 🐎.
