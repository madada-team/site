---
published: true
title: Lettre ouverte en soutien au programme NGI - l’Union européenne doit maintenir son aide aux logiciels libres
layout: post
author: L'équipe Ma Dada
category: articles
tags:
- NGI
- NLNet
- actualité
---

Le programme de financement européen NGI est en péril. C’est pourtant grâce à lui que de nombreux projets reposant sur du logiciel libre ont pu émerger et/ou se développer. Outre notre initiative, Ma Dada, on peut également citer, du côté des acteurs hexagonaux, Open Food Facts ou Framasoft.

L’association Open Knowledge France publie ci-dessous la [lettre ouverte écrite par Les petites singularités](https://ps.zoethical.org/pub/lettre-publique-aux-ncp-au-sujet-de-ngi/), et dont elle est signataire. 

---

L’ASBL des petites singularités est membre des consortia NGI0 depuis 5 ans et a participé à d’autres projets NGI. Cette année, à la lecture du drafts du Work Programme de Horizon Europe détaillant les programmes de financement de la commission européennes pour 2025, nous nous apercevons que les programmes Next Generation Internet ne sont plus mentionnés dans le Cluster 4.
Les programmes NGI ont démontré leur force et leur importance dans le soutien à l’infrastructure logicielle européenne, formant un instrument générique de financement des communs numériques qui doivent être rendus accessibles dans la durée. Nous sommes dans l’incompréhension de cette transformation, d’autant plus que le fonctionnement de NGI est efficace et économique puisqu’il soutient l’ensemble des projets de logiciel libre des plus petites initiatives aux mieux assises. La diversité de cet écosystème fait la grande force de l’innovation technologique européenne et le maintien de l’initiative NGI pour former un soutien structurel à ces projets logiciels, qui sont au cœur de l’innovation mondiale, permet de garantir la souveraineté d’une infrastructure européenne. Contrairement à la perception courante, les innovations techniques sont issues des communautés de programmeurs européens plutôt que nord américains, et le plus souvent issues de structures de taille réduite.

Le Cluster 4 allouait 27.00 millions d’euro au service de :

- “Human centric Internet aligned with values and principles commonly shared in Europe” ;
- “A flourishing internet, based on common building blocks created within NGI, that enables better control of our digital life” ;
- “A structured eco-system of talented contributors driving the creation of new internet commons and the evolution of existing internet commons”.
 Au nom de ces enjeux, ce sont plus de 500 projets qui ont reçu un financement NGI0 dans les 5 premières années d’exercice, ainsi que plus de 18 organisations collaborant à faire vivre ces consortiums européens.

NGI contribue à un vaste écosystème puisque la plupart du budget est dévolue au financement de tierces parties par le biais des open calls. Ils structurent des communs qui recouvrent l’ensemble de l’Internet, du hardware aux applications d’intégration verticales en passant par la virtualisation, les protocoles, les systèmes d’opération, les identités électroniques ou la supervision du trafic de données. Ce financement des tierces parties n’est pas renouvelé dans le programme actuel, ce qui laissera de nombreux projets sans ressources adéquates pour la recherche et l’innovation en Europe.

Par ailleurs, NGI permet des échanges et des collaborations à travers tous les pays de la zone EU et aussi avec ceux *widening countries* [^1], ce qui est actuellement une réussite tout autant qu’un progrès en cours, comme le fut le programme Erasmus avant nous. NGI0 est aussi une initiative qui participe à l’ouverture et à l’entretien de relations sur un temps plus long que les financements de projets. NGI encourage également à l’implémentation des projets financés par les biais de pilotes, et soutenait la collaboration au sein des initiatives, ainsi l’identification et la réutilisation d’éléments communs au travers des projets, l’interopérabilité notamment des systèmes d’identification, et la mise en place de modèles de développement intégrant les autres sources de financements aux différentes échelles en Europe.

Alors que les États-Unis d’Amérique, la Chine ou la Russie déploient des moyens publics et privés colossaux pour développer des logiciels et infrastructures captant massivement les données des consommateurs, l’Union Européenne ne peut pas se permettre ce renoncement. Les logiciels libres et open source tels que soutenus par les projets NGI depuis 2020 sont, par construction, à l’opposé des potentiels vecteurs d’ingérence étrangère. Ils permettent de conserver localement les données et de favoriser une économie et des savoirs-faire à l’échelle communautaire, tout en permettant à la fois une collaboration internationale. Ceci est d’autant plus indispensable dans le contexte géopolitique que nous connaissons actuellement. L’enjeu de la souveraineté technologique y est prépondérant et le logiciel libre permet d’y répondre sans renier la nécessité d’œuvrer pour la paix et la citoyenneté dans l’ensemble du monde numérique.

Dans ces perspectives, nous vous demandons urgemment de réclamer la préservation du programme NGI dans le programme de financement 2025.

[^1]: Tels que définis par Horizon Europe, les États Membres élargis sont la Bulgarie, la Croatie, Chypre, la République Tchèque, l’Estonie, la Grèce, la Hongrie, la Lettonie, la Lithuanie, Malte, la Pologne, le Portugal, la Roumanie, la Slovaquie et la Slovénie. Les pays associés élargies (sous conditions d’un accord d’association) l’Albanie, l’Arménie, la Bosnie Herzégovine, les Iles Feroé, la Géorgie, le Kosovo, la Moldavie, le Monténégro, le Maroc, la Macédoine du Nord, la Serbie, la Tunisie, la Turquie et l’Ukraine. Les régions élargies d’outre-mer sont: la Guadeloupe, la Guyane Française, la Martinique, La Réunion, Mayotte, Saint-Martin, Les Açores, Madère, les Iles Canaries.