---
published: true
title: Droit d'accès - la France à la traine
layout: post
author: L'équipe Ma Dada
category: articles
tags:
  - actualité
  - droit
---

L’association [Access Info Europe](https://www.access-info.org/2025-01-17/legal-recommendations-to-strengthen-access-to-documents-in-france/) vient de publier, en collaboration avec Open Knowledge France, un rapport mettant en perspective le droit d’accès aux documents administratifs français aux standards internationaux - et plus particulièrement la [Convention de Tromsø sur l'accès aux documents publics](https://rm.coe.int/168008483a).

Cette analyse, qui s’appuie notamment sur les données de Ma Dada, met en lumière des écarts significatifs, et portant notamment sur :

- Les trop nombreuses exceptions prévues par le droit français (visant par exemple les documents parlementaires ou les documents préparatoires).
- L’absence de dispositif de mise en balance entre l’intérêt pour le public de connaître des informations publiques et les secrets légaux (secret des affaires, secret des délibérations du gouvernement, etc.)
- Des barrières dans l’exercice du droit d’accès, notamment en termes de manque d’accompagnement des demandeurs ou de délais
- Les pouvoirs octroyés à la Commission d’accès aux documents administratifs (CADA)

Access Info Europe et Open Knowledge France invitent ainsi la France a mener différentes réformes, qui pourraient par exemple découler d’une ratification de la Convention de Tromsø.

Consulter le rapport (PDF, en anglais) [ici](https://www.access-info.org/wp-content/uploads/Access-to-Documents-in-France_-Recommendations-for-Legal-Reform-1.pdf).

Rachel Hanna, Directrice exécutive d'Access Info Europe :

« Le droit d'accès aux documents publics doit être garanti par un cadre juridique solide. Les lacunes et les faiblesses de la loi française créent des obstacles significatifs qui empêchent les citoyens d'exercer pleinement ce droit essentiel. Résoudre ces insuffisances est crucial pour améliorer la transparence, favoriser la confiance et promouvoir l'engagement démocratique. »

Xavier Berne, délégué général d'Open Knowledge France :

« Face aux dangers liés au complotisme et aux fausses informations, il est plus important que jamais que les administrations se conforment aux exigences constitutionnelles de transparence. Malheureusement, la loi française demeure mal appliquée depuis de nombreuses années et reste insuffisante à bien des égards. »
