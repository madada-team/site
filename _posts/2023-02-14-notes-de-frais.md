---
published: true
title: Les notes de frais des responsables publics, des documents administratifs communicables
layout: post
author: L'équipe Ma Dada
category: articles
tags:
- actualité
- justice
- demande d'accès
- notes de frais

---


**Bonne nouvelle pour la transparence ! Le Conseil d’État vient de juger, dans le cadre d’un litige qui opposait la ville de Paris au journaliste Stefan de Vries, que les notes de frais (de déplacement, de restauration…) des élus - comme des agents publics - étaient des documents administratifs, dès lors communicables à tous, sur demande.**

Au travers d'une [décision rendue le 8 février dernier](https://www.conseil-etat.fr/fr/arianeweb/CE/decision/2023-02-08/452521), le Conseil d’État retient en effet que “*des **notes de frais et reçus de déplacements ainsi que des notes de frais de restauration et reçus de frais de représentation d'élus locaux ou d'agents publics constituent des documents administratifs**, communicables à toute personne qui en fait la demande dans les conditions et sous les réserves prévues par les dispositions du code des relations entre le public et l'administration*”. 

Les juges ont notamment estimé que “*la communication des documents demandés, qui ont trait à l'activité de la maire de Paris dans le cadre de son mandat et des membres de son cabinet dans le cadre de leurs fonctions, ne saurait être regardée comme mettant en cause la vie privée de ces personnes*” (ce qui aurait pu être un motif justifiant la non-divulgation de ces informations). 

La ville de Paris a ainsi été condamnée à réexaminer la demande d’accès du journaliste (à laquelle elle s’opposait jusque-là) dans un délai d’un mois. Et pour cause, le Conseil d’État a considéré que les administrations ainsi sollicitées devaient malgré tout “*apprécier au cas par cas (...) si, eu égard à certaines circonstances particulières tenant au contexte de l'événement auquel un document se rapporte, la communication de ces dernières informations ou celle du motif de la dépense serait de nature, par exception, à porter atteinte aux secrets et intérêts protégés par [la loi, de type secret-défense ou protection des données personnelles, ndlr], justifiant alors leur occultation*”. Autrement dit, dans certains cas, a priori exceptionnels, les administrations peuvent refuser de divulguer des reçus ou notes de frais faisant apparaître des informations personnelles sur des tiers par exemple.

  

### A vos demandes d'accès !

  

Cette décision pose clairement que **toute personne est en droit de demander, puis d’obtenir** : 
 - des notes de frais de déplacements (et éventuellement les reçus afférents)
 - des notes de frais de restauration (et éventuellement les reçus afférents)
 - des notes de frais de représentation (éventuels frais vestimentaires, de coiffeur, d’informatique, de voyage, etc.)

Ceci tant pour **les élus à proprement parler (maires, présidents de région, ministres…) que leurs éventuels collaborateurs, ou même des agents publics de manière très large**.

Pour obtenir ces informations, **nous vous conseillons bien entendu d’utiliser notre plateforme citoyenne, [Ma Dada](https://madada.fr)**. 

Voici quelques conseils pour améliorer les chances de succès de vos demandes : 
 - Veillez à bien identifier le destinataire (tel ministère, ville, autorité administrative indépendante...)
 - Précisez ensuite le ou les documents demandés, ainsi que la personne à laquelle les documents se rapportent 
 - Spécifiez la période concernée

Si vous utilisez Ma Dada, nous vous invitons à reprendre le texte suivant (en lieu et place du texte de demande habituel) : 

```
Madame, Monsieur,
 
 Au titre du droit d’accès aux documents administratifs, 
 tel que prévu notamment par le Livre III du Code des 
 relations entre le public et l'administration, je 
 sollicite auprès de vous la communication des documents 
 administratifs suivants :
 - les notes de frais de déplacements, ainsi que les reçus 
 afférents, [du maire/des membres de cabinet du ministre/
 du directeur général des service de telle collectivité, 
 etc.], sur la période courant du [X/X/XX au X/X/XX]
 - les notes de frais de restauration, ainsi que les reçus 
 afférents, [du maire/des membres de cabinet du ministre/du 
 directeur général des service de telle collectivité, etc.], 
 sur la période courant du [X/X/XX au X/X/XX]
 - les notes de frais de représentation ainsi que les reçus 
 afférents, [du maire/des membres de cabinet du ministre/du 
 directeur général des service de telle collectivité, etc.], 
 sur la période courant du [X/X/XX au X/X/XX]
 Je vous rappelle à toutes fins utiles que le Conseil d’État 
 a déjà eu l’occasion de juger que “des notes de frais et 
 reçus de déplacements ainsi que des notes de frais de 
 restauration et reçus de frais de représentation d'élus 
 locaux ou d'agents publics constituent des documents 
 administratifs, communicables à toute personne qui en fait 
 la demande” (Conseil d'État 452521, lecture du 8 février 2023).
 
 Je souhaite recevoir ces documents dans un format numérique, 
 ouvert et réutilisable. 
 
 Comme le livre III du code des relations entre le public et 
 l’administration le prévoit lorsque le demandeur a mal 
 identifié la personne qui est susceptible de répondre à sa 
 requête, je vous prie de bien vouloir transmettre ma demande 
 au service qui détient les documents demandés si tel est le 
 cas.
 
 Veuillez agréer, Madame, Monsieur, l'expression de mes 
 sentiments distingués.
```

L'équipe de Ma Dada reste en soutien en cas de besoin : contact@madada.fr 🙌