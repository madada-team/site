--- 
published: true
title: "Comment faire ma demande d'accès aux documents administratifs? : un dépliant pour vous guider"
layout: post
author: Eda
category: articles
tags: 
- madada
- actualité
- atelier

---
![dépliant MaDada](https://gitlab.com/madada-team/site/-/raw/site/_posts/images/depliant-cada-plie.jpg)

En 2021, chez Ma Dada nous participions à un atelier citoyen de facilitation de l'accès à l'information publique, organisé par le [Donut Infolab](https://www.ledonut-marseille.com/) à Marseille, avec des acteurs locaux de la lutte pour le droit d'accès, et à destination de membres de collectifs, d'associations locales ou de simples citoyens. Cet atelier a été très enrichissant pour nous, et nous espérant sincèrement pouvoir en refaire d'autres, dans d'autres villes, sur des thématiques précises, avec vos collectifs, donc n'hésitez pas à nous solliciter.

À l'issue de cet atelier citoyen, nous en avons restitué l'essentiel sous forme d'un dépliant graphique intitulé « Comment faire ma demande d'accès aux documents administratifs? ».

Nous partageons ce document avec vous, en espérant qu'il puisse vous conduire, à galop, vers encore plus de transparence ;) 

![dépliant MaDada recto](https://gitlab.com/madada-team/site/-/raw/site/_posts/images/depliant-demande-cada01.png)
![dépliant MaDada verso](https://gitlab.com/madada-team/site/-/raw/site/_posts/images/depliant-demande-cada02.png)


Ce document est sous licence libre CC-By SA, vous pouvez l'utiliser, le copier, le diffuser, le passer à vos voisins, le modifier et diffuser ces modifications sous la même licence (ou autre licence compatible de type copyleft) en citant ces auteurs.


Merci au [Donut Infolab](https://www.ledonut-marseille.com/) de l'avoir organisé, à [la Base Marseille](labasemarseille.org/) de nous avoir accueillis, à [Atelier Graphique AD](https://amelie.desvernay.fr) d'avoir réalisé le travail graphique, et à tous les membres d'associations locales et de citoyens qui y ont participé ❤.


À très vite sur Ma Dada 🐎.
