---
layout: page
title: A propos
comments: yes
permalink: /about/
---


### Présentation de Madada.

Madada est un site web qui permet de faire des **demandes d'accès à un document administratif** conformément aux dispositions de la loi de 1978 dite Informatique et liberté. C'est un service opéré par l'association [Open Knowledge France](https://fr.okfn.org/) et outillé par le logiciel libre [Alaveteli](https://alaveteli.org/) édité par la fondation [MySociety](https://www.mysociety.org/).

L'objectif de la plateforme MaDada est de faciliter la démarche administrative de demande de communication de documents aux autorités publiques à l'aide d'une liste d'autorités publiques et de leurs coordonnées de contact et de formulaires de saisine. Il s'appuie sur la transparence des échanges avec les autorités publiques et sur l'accompagnement des bénévoles de l'association permettant d'aiguiller les demandeurs ou de les aider dans la formulation de leurs requêtes.

Les données ou documents récupérés dans le cadre de ces démarches sont rassemblées sur le [portail open data du gouvernement](https://www.data.gouv.fr/fr/organizations/madada/) 

## L'équipe

Pour faire fonctionner ce service l'équipe de Madada se compose de plusieurs joyeux drilles qui opérent dans les sous-couches du réseau ou au contact des utilisateur.rice.s

Laurent S. notre sysadmin, expert en configuration de serveurs mails et en paquets debian. Il travaille par ailleurs sur des projets à haute valeur sociétale mettant ses compétences en gestion des données au service de la Cause.

Eda N. notre animatrice de communauté, experte en logiciel libre elle assiste les demandeur.se.s dans leurs échanges avec les autorités publiques ou en concotant des tutoriels au petit oignons. Elle travaille par ailleurs comme développeuse ou hacktiviste pour des organisations engagées dans la défense des libertés publiques.

Pierre C. notre rédacteur en chef, expert en connaissance ouverte, président de l'association Open Knowledge France. Il travaille par ailleurs pour la banque mondiale sur l'acculturation à la gestion des données.

Samuel G. notre expert en droit d'accès en charge des relations avec nos partenaires oeuvrant pour l'amélioration de la réglementation en matière d'accès à l'information publique. Il travaille par ailleurs pour la coopérative datactivist et l'université de sciences politiques d'Aix-Marseille sur la culture de la donnée.

Pascal R. notre expert en autorité publique et en données ouvertes met son enthousiasme pour la transparence de l'action publique au service des utilisateur.rice.s. Il travaille par ailleurs dans la sphère publique dans le domaine de la gestion de la donnée.

## Sur les réseaux

Pour rentrer en contact avec l'équipe de Madada, vous pouvez utiliser ces différents moyens de contact : 

* compte Mastodon : [madada](https://mamot.fr/@madadafr)
* compte Twitter : [madada](https://twitter.com/madadafr)
* chaîne PeerTube : [madada](https://aperi.tube/accounts/madada)
* mail de contact : [mail](mailto:contact@madada.fr)

## Nos partenaires

La Cause de l'accès àl'information publique rassemble plusieurs associations qui oeuvrent localement au internationalement :
* [MySociety](https://www.mysociety.org/) : fondation anglaise opérant le site [whatdotheyknow](https://www.whatdotheyknow.com/) et à l'origine du développement du logiciel open source [Alaveteli](https://alaveteli.org/)
* [Transparencia](https://transparencia.be/) : association belge oeuvrant pour l'accès à l'information
* [Ouvre-boîte](https://ouvre-boite.org/) : association dont l'objet est d'obtenir l'accès et la publication effective des documents administratifs, et plus particulièrement des données, bases de données et codes sources, conformément aux textes en vigueur.  
